# Gitmoji 4 Eclipse - Update-Site

This project provides the update-site project of the **gitmoji4eclipse** plug-in.

The plug-in project is hosted [here](https://gitlab.com/gitmoji4eclipse/plugin), 
and the feature project [here](https://gitlab.com/gitmoji4eclipse/feature).

Get more information and an how-to on the 
[update-site page](https://gitmoji4eclipse.gitlab.io/update-site).